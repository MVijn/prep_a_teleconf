Hi,

Here list of things to think off:


# setup before
- light
  - setup 2 lamps with diffused light (left an right on ~45 degrees or more)
  - check colors of skins
- check on the modem / router
  - switch-off auto updates
  - enable  push services that detect connection loss
  - a G4 failback link would be nice
  - consider your gsm phone as hotspot
- if can work from a note book (prefered):
    - network cables
    - disable auto updates for the time of the meeting

# couple of hours before
- charge bluetooth headset
- charge phones / notebook
- test wifi / wired network (wired network is preffered)
- test conferencing platform, play with settings, options especially:
  - how enable disbale spound -> mute
  - volume settings -> high and low
  - enable/disable video
  - change microfoon settings 
  - raize hand / take token


# 15 min before
- close doors windows ventilation etc etc (noise reduction)
- connect  to network
- visit bathroom
- test audio/video setting
- have a spare (wired) headset nearby


# in the meeting
- switch microphone only on when speaking
- if the audio is bad maybe:
  - move closer to wifiacces-point or modem if you are on wifi
  - switch off video and test if it improves



